ここでは、laravelの初期画面の表示までを完了とする手順について記述。<br>
<br>
まず、プロジェクトに参加しソースをクローンします。<br>
<br>
laravelをまとめたディレクトリを「kohyo_project」と命名してますので、<br>
ここのリポジトリをクローンする際は「hackathon_project」などと名前を<br>
変更することをお勧めします！！<br>
ディレクトリ構成のイメージとしては<br>
hackathon_project<br>
　∟kohyo_project(laravel)<br>
　　∟app<br>
　　∟bootstrap<br>
　　...<br>
<br>
クローンが完了したら、ターミナル （winならgib bashなど）で<br>
hackathon_project配下までいき<br>
<code>$ vagrant up</code><br>
<br>
upが完了したらvendorを作ります。要、composer!<br>
・ssh繋ぐ<br>
<code>$ vagrant ssh</code><br>
・laravelのところまで移動<br>
<code>$ cd hackathon_project/kohyo_project</code><br>
・vendor作れ<br>
<code>$ composer update</code><br>
<br>
完了したら.envにアプリケーションキーを作る<br>
<code>$ php artisan key:generate</code><br>
<br>
sshを切ってvagrantを再起動<br>
<code>$ vagrant reload</code><br>
<br>
これでひとまず<br>
http://192.168.33.10/<br>
にアクセスすればlaravelのTOPが開きます。<br>
